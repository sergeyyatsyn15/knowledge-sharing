CREATE TABLE knowledge_user
(
    id                bigint NOT NULL PRIMARY KEY,
    name              varchar(60)   not null,
    surname           varchar(60)   not null,
    password          varchar(255)  not null,
    email             varchar(40)   not null,
    role_name         varchar(20)   not null,
    course_name       varchar(20)   not null,
    avatar_id         bigint,
    creation_date     timestamp DEFAULT now(),
    modification_date timestamp DEFAULT now(),
    CONSTRAINT FK_User FOREIGN KEY (role_name)
        REFERENCES role (name),
    CONSTRAINT FK_Course FOREIGN KEY (course_name)
        REFERENCES course (name),
    CONSTRAINT FK_Avatar FOREIGN KEY (avatar_id)
        REFERENCES avatar (id)
);
