CREATE TABLE resource
(
    id          bigint  NOT NULL PRIMARY KEY,
    title       varchar not null,
    creator     bigint  not null,
    recommended boolean default false,
    rating_sum   int,
    rating_count int,
    url         varchar not null,
--     theme       bigint  not null,
    type        varchar(20) not null,
--     user_id     bigint not null ,
    theme    bigint not null ,

    CONSTRAINT FK_Resource FOREIGN KEY (creator)
        REFERENCES knowledge_user (id),
    CONSTRAINT FK_Theme FOREIGN KEY (theme)
        REFERENCES theme (id),
    CONSTRAINT FK_Type FOREIGN KEY (type)
        REFERENCES resource_type (name)
);
