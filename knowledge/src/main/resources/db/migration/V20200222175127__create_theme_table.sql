CREATE TABLE theme
(
    id      bigint  NOT NULL PRIMARY KEY,
    title   varchar not null,
    creator bigint  not null,
    CONSTRAINT FK_Theme FOREIGN KEY (creator)
        REFERENCES knowledge_user (id)
);
