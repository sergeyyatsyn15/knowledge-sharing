package com.knowledge.auth;

import com.google.common.collect.Lists;
import com.knowledge.auth.security.ApplicationUserRole;
import com.knowledge.dto.UserDtoWithPassword;
import com.knowledge.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.knowledge.auth.security.ApplicationUserRole.values;

@RequiredArgsConstructor
@Repository("applicationUserDao")
public class ApplicationUserDaoService implements ApplicationUserDao {

    private final PasswordEncoder passwordEncoder;
    private final UserService userService;

    @Override
    public Optional<ApplicationUser> selectApplicationUserByUsername(String username) {
        return getApplicationUsers()
                .stream()
                .filter(applicationUser -> username.equals(applicationUser.getUsername()))
                .findFirst();
    }

    private List<ApplicationUser> getApplicationUsers() {
        List<UserDtoWithPassword> users = userService.getAllWithPassword();
        List<ApplicationUser> applicationUsers = Lists.newArrayList();
        for (UserDtoWithPassword userDto : users) {
            ApplicationUserRole role = null;
            for (int i = 0; i < values().length; i++) {
                if (values()[i].name().equals(userDto.getRole().getName())) {
                    role = values()[i];
                    break;
                }
            }
            applicationUsers.add(
                    new ApplicationUser(
                            userDto.getId(),
                            userDto.getEmail(),
                            userDto.getPassword(),
                            role.getGrantedAuthorities(),
                            true,
                            true,
                            true,
                            true
                    )
            );
        }

        return applicationUsers;
    }

}
