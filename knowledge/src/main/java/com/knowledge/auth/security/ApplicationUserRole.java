package com.knowledge.auth.security;

import com.google.common.collect.Sets;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

import static com.knowledge.auth.security.ApplicationUserPermission.*;


public enum ApplicationUserRole {
    MENTOR(Sets.newHashSet()),

    STUDENT(Sets.newHashSet(
            ROLE_GET_ALL,
            ROLE_GET_BY_NAME,
            USER_GET_ALL,
            USER_GET_LIST_BY_SUBSTRING,
            USER_CREATE,
            USER_GET_BY_ID,
            USER_UPDATE,
            USER_DELETE,
            USER_GET_LIST_BY_SUBSTRING));

    private final Set<ApplicationUserPermission> permissions;

    ApplicationUserRole(Set<ApplicationUserPermission> permissions) {
        this.permissions = permissions;
    }

    public Set<ApplicationUserPermission> getPermissions() {
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getGrantedAuthorities() {
        Set<SimpleGrantedAuthority> permissions = getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());
        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return permissions;
    }
}
