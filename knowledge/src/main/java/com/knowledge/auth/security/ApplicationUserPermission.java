package com.knowledge.auth.security;

public enum ApplicationUserPermission {
    ROLE_GET_ALL("role:getAllRoles"),
    ROLE_GET_BY_NAME("role:getRoleByName"),
    USER_GET_ALL("user:getAllUsers"),
    USER_GET_LIST_BY_SUBSTRING("user:getUserListBySubstring"),
    USER_CREATE("user:create"),
    USER_GET_BY_ID("user:getUserById"),
    USER_UPDATE("user:update"),
    USER_DELETE("user:delete");


    private final String permission;

    ApplicationUserPermission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
