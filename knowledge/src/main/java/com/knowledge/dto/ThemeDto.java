package com.knowledge.dto;

import com.knowledge.entity.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ThemeDto {

    private Long id;

    private String title;

    private User creator;
}
