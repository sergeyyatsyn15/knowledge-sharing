package com.knowledge.dto;

import com.knowledge.entity.ResourceType;
import com.knowledge.entity.Theme;
import com.knowledge.entity.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ResourceDto {

    private Long id;

    private String title;

    private User creator;

    private Boolean recommended;

    private int averageRating;

    private int ratingSum;

    private int ratingCount;

    private String url;

    private Theme theme;

    private ResourceType type;
}
