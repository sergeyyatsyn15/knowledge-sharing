package com.knowledge.dto;

import com.knowledge.entity.Avatar;
import com.knowledge.entity.Course;
import com.knowledge.entity.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class UserDto {

    @Size(max = 60)
    private String name;

    @Size(max = 60)
    private String surname;

    @Email
    @Size(min = 5, max = 254)
    private String email;

    private Avatar avatar;

    private Role role;

    private Course course;
}
