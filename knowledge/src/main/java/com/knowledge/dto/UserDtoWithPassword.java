package com.knowledge.dto;

import com.knowledge.entity.Avatar;
import com.knowledge.entity.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class UserDtoWithPassword {

    private Long id;

    @Size(max = 60)
    private String name;

    @Size(max = 60)
    private String surname;

    @Email
    @Size(min = 5, max = 254)
    private String email;

    private String password;

    private Role role;


    private Avatar avatar;

    private LocalDateTime creationDate;

    private LocalDateTime modificationDate;

}
