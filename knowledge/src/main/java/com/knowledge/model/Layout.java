package com.knowledge.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Layout {

    UK("йцукенгшщзхїфівапролджєячсмитьбю."),
    EN("qwertyuiop[]asdfghjkl;'zxcvbnm,./"),
    RU("йцукенгшщзхъфывапролджэячсмитьбю."),
    UNKNOWN("");

    private final String characters;
}
