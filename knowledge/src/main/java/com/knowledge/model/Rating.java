package com.knowledge.model;

import lombok.Data;

@Data
public class Rating {

    private Long resourceId;

    private int rating;

    private int averageRating;

    private int ratingCount;
}
