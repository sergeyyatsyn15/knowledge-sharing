package com.knowledge.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "theme")
public class Theme implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "themeseq")
    @SequenceGenerator(name = "themeseq", sequenceName = "id_seq")
    private Long id;

    @Column(nullable = false)
    private String title;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "theme")
    private List<Resource> resources;

    @OneToOne
    @JoinColumn(name = "creator")
    private User creator;
}
