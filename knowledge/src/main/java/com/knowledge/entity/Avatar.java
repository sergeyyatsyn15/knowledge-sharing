package com.knowledge.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "avatar")
public class Avatar {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "avatarseq")
    @SequenceGenerator(name = "avatarseq", sequenceName = "id_seq")
    private Long id;

    @Column(nullable = false)
    private String url;
}
