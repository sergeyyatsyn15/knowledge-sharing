package com.knowledge.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "resource")
public class Resource implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "resourceseq")
    @SequenceGenerator(name = "resourceseq", sequenceName = "id_seq")
    private Long id;

    @Column(nullable = false)
    private String title;

    @OneToOne
    @JoinColumn(name = "creator")
    private User creator;

    @Column(columnDefinition = "false")
    private Boolean recommended;

    @Column
    private int ratingSum;

    @Column
    private int ratingCount;

    @Column(nullable = false)
    private String url;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "theme")
    private Theme theme;

    @OneToOne
    @JoinColumn(name = "type")
    private ResourceType type;
}
