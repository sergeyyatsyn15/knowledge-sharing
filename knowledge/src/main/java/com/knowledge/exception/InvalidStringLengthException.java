package com.knowledge.exception;

public class InvalidStringLengthException extends ValidateException {

    private static final String MESSAGE = "Invalid string length";

    public InvalidStringLengthException(String string, int requiredMinLength, int requiredMaxLength, int foundLength) {
        super(MESSAGE, string, requiredMinLength, requiredMaxLength, foundLength);
    }
}
