package com.knowledge.exception;

public class NotValidEmailException extends ValidateException {

    private static final String MESSAGE = "Not valid email";

    public NotValidEmailException() {
        super(MESSAGE);
    }
}
