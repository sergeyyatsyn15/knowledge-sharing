package com.knowledge.exception;

public class NotFoundException extends Exception {

    private static final String MESSAGE = "Could not find record where id = %d";

    public NotFoundException(Long id) {
        super(String.format(MESSAGE, id));
    }

    public NotFoundException(String message) {
        super(message);
    }
}
