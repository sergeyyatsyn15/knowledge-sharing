package com.knowledge.exception;

public class NullFieldException extends Exception {

    public static final String MESSAGE = "Null field exception";

    public NullFieldException() {
        super(MESSAGE);
    }

    public NullFieldException(String message) {
        super(MESSAGE + " : " + message);
    }

}
