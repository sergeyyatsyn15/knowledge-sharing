package com.knowledge.exception;

public class ValidateException extends Exception {

    private final Object[] parameters;

    public ValidateException(String message) {
        super(message);
        parameters = new Object[0];
    }

    public ValidateException(String message, Object... parameters) {
        super(message);
        this.parameters = parameters;
    }

    public Object[] getParameters() {
        return parameters;
    }
}
