package com.knowledge.service;

import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MailService {

    private final JavaMailSender javaMailSender;

    public void sendEmail(String email, String titleMessage, String message) {

        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setFrom("test6455@ukr.net");
        msg.setTo(email);

        msg.setSubject(titleMessage);
        msg.setText(message);

        javaMailSender.send(msg);
    }
}
