package com.knowledge.service;

import com.knowledge.dto.ThemeDto;
import com.knowledge.entity.Role;
import com.knowledge.entity.Theme;
import com.knowledge.entity.User;
import com.knowledge.exception.NotFoundException;
import com.knowledge.repository.ThemeRepository;
import com.knowledge.util.ModelMapperUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ThemeService {

    private final ThemeRepository themeRepository;
    private final RoleService roleService;
    private final UserService userService;

    public List<ThemeDto> getAllByRole(String roleName) {
        Role currRole = roleService.getRoleByName(roleName);
        List<Theme> allByCreatorRole = themeRepository.findAllByCreatorRole(currRole);
        return allByCreatorRole.stream()
                .map(this::mapToDto)
                .collect(Collectors.toList());
    }

    public List<Theme> findAllBySubString(String input) {
        return themeRepository.findAllBySubString(input);
    }

    public Optional<Theme> getThemeById(Long id) {
      return themeRepository.findById(id);
    }

    public void save(Theme theme, Principal principal) throws NotFoundException {
        User user = userService.getUserByEmail(principal.getName());
        theme.setCreator(user);
        themeRepository.save(theme);
    }

    private ThemeDto mapToDto(Theme theme) {
        return ModelMapperUtil.map(theme, ThemeDto.class);
    }

    public Optional<Theme> getById(Long id) {
        return themeRepository.findById(id);
    }
}
