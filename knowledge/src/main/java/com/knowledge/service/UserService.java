package com.knowledge.service;


import com.knowledge.dto.UserDto;
import com.knowledge.dto.UserDtoWithPassword;
import com.knowledge.entity.User;
import com.knowledge.exception.NotFoundException;
import com.knowledge.repository.UserRepository;
import com.knowledge.util.ModelMapperUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public List<UserDto> getAll() {
        return toUsersDto(userRepository.findAll(getSorterBySurnameAndName()));
    }

    public UserDto getUserDtoByEmail(String email) throws NotFoundException {
        return toUserDto(userRepository.findByEmail(email)
                .orElseThrow(() -> new NotFoundException("User with email: " + email + " does not exist!")));
    }

    public User getUserByEmail(String email) throws NotFoundException {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new NotFoundException("User with email: " + email + " does not exist!"));
    }

    public List<User> findAllBySubString(String input) {
        return userRepository.findAllBySubString(input);
    }

    public List<UserDtoWithPassword> getAllWithPassword() {
        return mapAllToDtoWithPassword(userRepository.findAll());
    }

    public Optional<UserDto> getById(Long id) {
        User user = this.userRepository.findById(id).orElseThrow();
        return Optional.of(toUserDto(user));
    }

    @Transactional
    public void save(User user) {
        String encryptedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encryptedPassword);
        userRepository.save(user);
    }

    @Transactional
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    private Sort getSorterBySurnameAndName() {
        return Sort.by("surname").and(Sort.by("name"));
    }


    private List<UserDto> toUsersDto(List<User> users) {
        return ModelMapperUtil.mapAll(users, UserDto.class);
    }

    private User toUser(UserDto userDto) {
        return mapFromDto(userDto);
    }

    private UserDto toUserDto(User user) {
        return mapToDto(user);
    }

    private UserDto toUserWithPasswordDto(User user) {
        return mapToDto(user);
    }

    private UserDto mapToDto(User user) {
        return ModelMapperUtil.map(user, UserDto.class);
    }

    private List<UserDtoWithPassword> mapAllToDtoWithPassword(List<User> users) {
        return ModelMapperUtil.mapAll(users, UserDtoWithPassword.class);
    }

    private User mapFromDto(UserDto userDto) {
        return ModelMapperUtil.map(userDto, User.class);
    }
}
