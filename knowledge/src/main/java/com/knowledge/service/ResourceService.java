package com.knowledge.service;

import com.knowledge.dto.ResourceDto;
import com.knowledge.entity.Resource;
import com.knowledge.entity.Theme;
import com.knowledge.entity.User;
import com.knowledge.exception.NotFoundException;
import com.knowledge.model.Rating;
import com.knowledge.repository.ResourcesRepository;
import com.knowledge.util.ModelMapperUtil;
import lombok.RequiredArgsConstructor;


import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class ResourceService {

    private final ResourcesRepository resourcesRepository;
    private final ThemeService themeService;
    private final UserService userService;

    public List<ResourceDto> getAllResource() {
        Sort.Order order = Sort.Order.desc("recommended");
        List<ResourceDto> resourceDtoList = toResourcesDto(resourcesRepository.findAll());
        return resourceDtoList
                .stream()
                .peek(resourceDto -> resourceDto.setAverageRating(averageRating(resourceDto.getRatingSum(), resourceDto.getRatingCount())))
                .collect(Collectors.toList());
    }

    public List<Resource> findAllBySubString(String input) {
        return resourcesRepository.findAllBySubString(input);
    }

    public Optional<ResourceDto> getResourceDtoById(Long id) {
        Resource resource = resourcesRepository.findById(id).orElseThrow();
        int averageRating = averageRating(resource.getRatingSum(), resource.getRatingCount());
        ResourceDto resourceDto = toResourceDto(resource);
        resourceDto.setAverageRating(averageRating);
        return Optional.of(resourceDto);
    }

    public Optional<Resource> getResourceById(Long id) {
        return resourcesRepository.findById(id);
    }

    public List<ResourceDto> getAllByTheme(Long id) {
        Theme theme = themeService.getThemeById(id).orElseThrow();
        List<Resource> allByTheme = resourcesRepository.findAllByTheme(theme);
        return allByTheme.stream()
                .map(this::mapToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public Optional<ResourceDto> saveResource(Resource resource, Principal principal) throws NotFoundException {
        User user = userService.getUserByEmail(principal.getName());
        resource.setCreator(user);
        resourcesRepository.save(resource);
        int averageRating = averageRating(resource.getRatingSum(), resource.getRatingCount());
        ResourceDto resourceDto = toResourceDto(resource);
        resourceDto.setAverageRating(averageRating);
        return Optional.of(resourceDto);
    }

    @Transactional
    public Rating saveResourceRating(Rating rating) {
        Resource resource = resourcesRepository.findById(rating.getResourceId()).orElseThrow();
        int newRating = resource.getRatingSum() + rating.getRating();
        resource.setRatingSum(newRating);
        resource.setRatingCount(resource.getRatingCount() + 1);
        resourcesRepository.save(resource);
        Resource updateResource = resourcesRepository.findById(resource.getId()).orElseThrow();
        int averageRating = averageRating(updateResource.getRatingSum(), updateResource.getRatingCount());
        return setRating(rating, averageRating, resource.getRatingCount());
    }

    private int averageRating(int ratingSum, int ratingCount) {
        return (int) Math.ceil(ratingSum / (double) ratingCount);
    }

    private Rating setRating(Rating rating, int averageRating, int ratingCount) {
        rating.setAverageRating(averageRating);
        rating.setRatingCount(ratingCount);
        return rating;
    }

    private List<ResourceDto> toResourcesDto(List<Resource> resources) {
        return ModelMapperUtil.mapAll(resources, ResourceDto.class);
    }

    private ResourceDto toResourceDto(Resource resource) {
        return mapToDto(resource);
    }

    private ResourceDto mapToDto(Resource resource) {
        return ModelMapperUtil.map(resource, ResourceDto.class);
    }
}
