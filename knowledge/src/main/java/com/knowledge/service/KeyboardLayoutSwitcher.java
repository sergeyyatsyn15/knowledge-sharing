package com.knowledge.service;


import com.knowledge.model.Layout;
import org.springframework.stereotype.Service;

import java.util.function.Predicate;
import java.util.stream.Stream;

import static com.knowledge.model.Layout.*;

@Service
public class KeyboardLayoutSwitcher {

    public String switchKeyboardLayout(String query) {
        query = query.toLowerCase();
        Layout currentLayout = getCurrentLayout(query);
        StringBuilder translatedQuery = new StringBuilder();
        for (char character : query.toCharArray()) {
            if (currentLayout.equals(EN)) {
                translatedQuery.append(translate(EN, UK, character));
            } else if (currentLayout.equals(UK)) {
                translatedQuery.append(translate(UK, EN, character));
            } else if (currentLayout.equals(RU)) {
                translatedQuery.append(translate(RU, EN, character));
            } else return query;
        }

        return translatedQuery.toString();
    }

    private Layout getCurrentLayout(String query) {
        if (inLayout(UK).test(query)) {
            return UK;
        } else if (inLayout(EN).test(query)) {
            return EN;
        } else if (inLayout(RU).test(query)) {
            return RU;
        }

        return UNKNOWN;
    }

    private Predicate<String> inLayout(Layout layout) {
        return query -> Stream.of(query.split(""))
                .allMatch(s -> layout.getCharacters().contains(s));
    }

    private Character translate(Layout from, Layout to, Character character) {
        return to.getCharacters().charAt(from.getCharacters().indexOf(character));
    }
}
