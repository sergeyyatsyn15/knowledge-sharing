package com.knowledge.controller;

import com.knowledge.dto.ResourceDto;
import com.knowledge.entity.Resource;
import com.knowledge.exception.NotFoundException;
import com.knowledge.model.Rating;
import com.knowledge.service.KeyboardLayoutSwitcher;
import com.knowledge.service.ResourceService;
import com.knowledge.util.ModelMapperUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("api/resources")
public class ResourceController {

    private final ResourceService resourceService;
    private final KeyboardLayoutSwitcher keyboardLayoutSwitcher;

    @GetMapping("/by")
    public ResponseEntity<List<ResourceDto>> getResourceListBySubstring(@RequestParam String input) {
        List<Resource> allBySubString = resourceService.findAllBySubString('%' + input + '%');
        if (allBySubString.isEmpty()) {
            String switchedLayout = keyboardLayoutSwitcher.switchKeyboardLayout(input);
            allBySubString = resourceService.findAllBySubString('%' + switchedLayout + '%');
        }
        return ResponseEntity.ok(mapAllToResourceDto(allBySubString));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResourceDto> getResourceById(@PathVariable Long id)
            throws NotFoundException {
        Optional<ResourceDto> resourceDto = resourceService.getResourceDtoById(id);
        return ResponseEntity.ok(resourceDto.orElseThrow(() ->
                new NotFoundException("Resource with id=" + id + " not found")));
    }

    @GetMapping("/themes/{id}")
    public ResponseEntity<List<ResourceDto>> getResourceByTheme(@PathVariable Long id) {
        return ResponseEntity.ok(resourceService.getAllByTheme(id));
    }

    @GetMapping
    public ResponseEntity<List<ResourceDto>> getAllResources() {
        return ResponseEntity.ok(resourceService.getAllResource());
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResourceDto> updateResource(@RequestBody Resource resource, Principal principal)
            throws NotFoundException {
        ResourceDto resourceDto = resourceService.saveResource(resource, principal).orElseThrow();
        return ResponseEntity.status(HttpStatus.OK).body(resourceDto);
    }

    @PostMapping("/rating")
    public ResponseEntity<Rating> updateRating(@RequestBody Rating rating) {
        Rating newRating = resourceService.saveResourceRating(rating);
        return ResponseEntity.status(HttpStatus.OK).body(newRating);
    }

    @PostMapping
    public ResponseEntity<ResourceDto> createResource(@RequestBody Resource resource, Principal principal)
            throws NotFoundException {
        ResourceDto resourceDto = resourceService.saveResource(resource, principal).orElseThrow();
        return ResponseEntity.status(HttpStatus.CREATED).body(resourceDto);
    }

    private List<ResourceDto> mapAllToResourceDto(List<Resource> resources) {
        return ModelMapperUtil.mapAll(resources, ResourceDto.class);
    }
}
