package com.knowledge.controller;

import com.knowledge.dto.ThemeDto;
import com.knowledge.entity.Theme;
import com.knowledge.exception.NotFoundException;
import com.knowledge.service.KeyboardLayoutSwitcher;
import com.knowledge.service.ThemeService;
import com.knowledge.util.ModelMapperUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("api/themes")
public class ThemeController {

    private final ThemeService themeService;
    private final KeyboardLayoutSwitcher keyboardLayoutSwitcher;

    @GetMapping("/role/{role}")
    public ResponseEntity<List<ThemeDto>> getAllThemeByRole(@PathVariable String role) {
        return ResponseEntity.ok(themeService.getAllByRole(role));
    }

    @GetMapping("/by")
    public ResponseEntity<List<ThemeDto>> getThemeListBySubstring(@RequestParam String input) {
        List<Theme> allBySubString = themeService.findAllBySubString('%' + input + '%');
        if (allBySubString.isEmpty()) {
            String switchedLayout = keyboardLayoutSwitcher.switchKeyboardLayout(input);
            allBySubString = themeService.findAllBySubString('%' + switchedLayout + '%');
        }
        return ResponseEntity.ok(mapAllToThemeDto(allBySubString));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Theme> getThemeById(@PathVariable Long id) throws NotFoundException {
        Optional<Theme> theme = themeService.getById(id);
        return ResponseEntity.ok(theme.orElseThrow(() ->
                new NotFoundException("Theme with id=" + id + " not found")));
    }

    @PostMapping()
    public ResponseEntity<Theme> create(@RequestBody Theme theme, Principal principal)
            throws NotFoundException {
        themeService.save(theme, principal);
        return ResponseEntity.status(HttpStatus.CREATED).body(theme);
    }

    private List<ThemeDto> mapAllToThemeDto(List<Theme> themes) {
        return ModelMapperUtil.mapAll(themes, ThemeDto.class);
    }
}
