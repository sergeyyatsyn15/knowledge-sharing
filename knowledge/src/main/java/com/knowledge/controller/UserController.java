package com.knowledge.controller;

import com.knowledge.dto.UserDto;
import com.knowledge.entity.User;
import com.knowledge.exception.NotFoundException;
import com.knowledge.service.KeyboardLayoutSwitcher;
import com.knowledge.service.UserService;
import com.knowledge.util.ModelMapperUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Optional;


@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("api/users")
public class UserController {

    private final UserService userService;
    private final KeyboardLayoutSwitcher keyboardLayoutSwitcher;

    @GetMapping
    //@PreAuthorize("hasAuthority('user:getAllUsers')")
    public ResponseEntity<List<UserDto>> getAllUsers() {
        return ResponseEntity.ok(userService.getAll());
    }

    @GetMapping("/current")
    public ResponseEntity<UserDto> getCurrentUser(Principal principal) throws NotFoundException {
        return ResponseEntity.ok(userService.getUserDtoByEmail(principal.getName()));
    }

    @GetMapping("/by")
    //@PreAuthorize("hasAuthority('user:getUserListBySubstring')")
    public ResponseEntity<List<UserDto>> getUserListBySubstring(@RequestParam String input) {
        List<User> allBySubString = userService.findAllBySubString('%' + input + '%');
        if (allBySubString.isEmpty()) {
            String switchedLayout = keyboardLayoutSwitcher.switchKeyboardLayout(input);
            allBySubString = userService.findAllBySubString('%' + switchedLayout + '%');
        }
        return ResponseEntity.ok(mapAllToUserDto(allBySubString));
    }

    @PostMapping
    //@PreAuthorize("hasAuthority('user:create')")
    public ResponseEntity<User> create(@RequestBody User user) {
        userService.save(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(user);
    }

    @GetMapping("/{id}")
    //@PreAuthorize("hasAuthority('user:getUserById')")
    public ResponseEntity<UserDto> getUserById(@PathVariable Long id) throws NotFoundException {
        Optional<UserDto> userDto = userService.getById(id);
        return ResponseEntity.ok(userDto.orElseThrow(() ->
                new NotFoundException("User with id=" + id + " not found")));
    }

    @PutMapping("/{id}")
    //@PreAuthorize("hasAuthority('user:update')")
    public ResponseEntity<User> update(@PathVariable Long id, @RequestBody User user) {
        user.setId(id);
        userService.save(user);
        return ResponseEntity.status(HttpStatus.OK).body(user);
    }

    @DeleteMapping("/{id}")
    //@PreAuthorize("hasAuthority('user:delete')")
    public ResponseEntity delete(@PathVariable Long id) {
        userService.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    private List<UserDto> mapAllToUserDto(List<User> users) {
        return ModelMapperUtil.mapAll(users, UserDto.class);
    }
}
