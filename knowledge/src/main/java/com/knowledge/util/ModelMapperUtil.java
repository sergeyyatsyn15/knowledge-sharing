package com.knowledge.util;

import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

public class ModelMapperUtil {

    public static <T> T map(Object source, Class<T> destination) {
        return new ModelMapper().map(source, destination);
    }

    public static <T, D> List<D> mapAll(List<T> source, Class<D> destination) {
        return source.stream()
                .map(t -> ModelMapperUtil.map(t, destination))
                .collect(Collectors.toList());
    }

}
