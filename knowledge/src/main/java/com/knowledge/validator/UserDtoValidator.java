package com.knowledge.validator;


import com.knowledge.dto.UserDto;
import com.knowledge.exception.InvalidStringLengthException;
import com.knowledge.exception.NotValidEmailException;
import com.knowledge.exception.NullFieldException;
import com.knowledge.exception.ValidateException;

import java.lang.reflect.Field;

public class UserDtoValidator {

    public static void validate(UserDto userDto) throws ValidateException, IllegalAccessException, NullFieldException {
        checkForNullFields(userDto);
        isValidStringField(userDto.getName(), 3, 20, "Name");
        isValidStringField(userDto.getSurname(), 3, 20, "Surname");
        if (!isValidEmailAddress(userDto.getEmail())) {
            throw new NotValidEmailException();
        }
    }

    public static boolean isValidEmailAddress(String email) {
        return email.matches(ValidatorPattern.email);
    }

    private static void isValidStringField(String string, int minLength, int maxLength, String fieldName) throws InvalidStringLengthException {
        if (string.length() < minLength) {
            throw new InvalidStringLengthException(fieldName, minLength, maxLength, string.length());
        }
        if (string.length() > maxLength) {
            throw new InvalidStringLengthException(fieldName, minLength, maxLength, string.length());
        }
    }


    private static void checkForNullFields(UserDto userDto) throws IllegalAccessException, NullFieldException {
        for (Field f : userDto.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            if (!f.getName().equals("id") && !f.getName().equals("avatar")
                    && !f.getName().equals("applicationUser")
                    && f.get(userDto) == null) {

                throw new NullFieldException(f.getName());
            }
        }
    }


}
