package com.knowledge.repository;

import com.knowledge.entity.Resource;
import com.knowledge.entity.Role;
import com.knowledge.entity.Theme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ThemeRepository extends JpaRepository<Theme, Long> {

    List<Theme> findAllByCreatorRole(Role role);

    @Query(
            "select t " +
                    "from Theme t " +
                    "where " +
                    "lower(t.title) like :query"
    )
    List<Theme> findAllBySubString(@Param("query") String query);
}
