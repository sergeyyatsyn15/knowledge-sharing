package com.knowledge.repository;

import com.knowledge.entity.Resource;
import com.knowledge.entity.Theme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResourcesRepository extends JpaRepository<Resource, Long> {

    List<Resource> findAllByTheme(Theme theme);

    @Query(
            "select r " +
                    "from Resource r " +
                    "where " +
                    "lower(r.title) like :query"
    )
    List<Resource> findAllBySubString(@Param("query") String query);
}
