package com.knowledge.repository;


import com.knowledge.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);

    @Query(
            "select u " +
                    "from User u " +
                    "where " +
                    "lower(concat(u.name, ' ', u.surname, ' ')) like :query or " +
                    "lower(concat(u.surname, ' ', u.name, ' ')) like :query or " +
                    "lower(u.email) like :query"
    )
    List<User> findAllBySubString(@Param("query") String query);

}
